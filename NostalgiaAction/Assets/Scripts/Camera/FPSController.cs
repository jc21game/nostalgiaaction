﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//フレームレートを管理するクラス
public class FPSController : MonoBehaviour
{
    [SerializeField] int fps_; //一秒間の更新量

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Awake()
    {
        Application.targetFrameRate = fps_; //フレームレートを設定
    }
}
