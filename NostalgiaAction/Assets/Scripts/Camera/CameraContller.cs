﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//カメラの動きを管理
public class CameraContller : MonoBehaviour
{
    [SerializeField] private GameObject player_ = null; //プレイヤーの情報を参照

    // Start is called before the first frame update
    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        //とりあえずカメラはプレイヤーに追従
        transform.position = transform.position = new Vector3(player_.transform.position.x, 0, -10);
    }

    
}
