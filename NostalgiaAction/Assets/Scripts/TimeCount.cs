﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeCount : MonoBehaviour
{
    [SerializeField] int minute_;    //二桁で分の設定
    [SerializeField] float seconds_; //二桁で秒の設定
    private float oldSeconds_; //前フレームの秒を記録
    private Text timerText_;   //タイマーを表示に使用するテキストUIをアタッチ

    // Start is called before the first frame update
    void Start()
    {
        //値の初期化
        minute_ = 2;
        seconds_ = 0f;
        oldSeconds_ = 0f;

        //コンポーネントの取得
        timerText_ = GetComponentInChildren<Text>(); //スクリプトをアタッチしたオブジェクトから取得
    }

    // Update is called once per frame
    void Update()
    {
        seconds_ -= Time.deltaTime; //更新してく
        if (seconds_ <= 0f) 
        {
            //繰り下げる
            //20:00 -> 19:60
            minute_--;
            seconds_ = 60;
        }

        //分が0以下つまり
        //-01:60みたいなときにゲームオーバーに遷移
        if (minute_ < 0)
        {
            SceneManager.LoadScene("GameOVerScene");
        }

        //値が変わった時だけテキストUIを更新
        if ((int)seconds_ != (int)oldSeconds_)
        {
            timerText_.text = minute_.ToString("00") + ":" + ((int)seconds_).ToString("00");
        }

        oldSeconds_ = seconds_; //記憶する
    }

    //分を取得
    //戻値 現在の分
    public int GetMinute()
    {
        return minute_;
    }

    //秒を取得
    //戻値 現在の秒
    public float GetSeconds()
    {
        return seconds_;
    }

}
