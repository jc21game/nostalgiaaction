﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//弓矢の挙動を管理するクラス
public class Ya : MonoBehaviour
{
    private float xSpeed_ = 10f; //矢の速さ
    private float ySpeed_ = 3f;
    private const float GRAVITY = 0.3f;
    Vector3 YaPos_; 
    Vector3 baseVec_ = new Vector3(1.0f, 0, 0); //基準ベクトル


    private void Start()
    {
        YaPos_ = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 diff = transform.position - YaPos_; //進んでいる方向
        Vector3 moveVec = new Vector3(xSpeed_, ySpeed_, 0); //矢の速度
        Vector3 normalMoveVec = Vector3.Normalize(moveVec); //角度を求めるので正規化を行う
        
        transform.position += moveVec * Time.deltaTime; //移動

        float zAngle = Vector3.Angle(baseVec_, normalMoveVec);

        //矢が下降するとき
        //基準ベクトルとの角度はマイナスになる
        if (ySpeed_<0)
        {
            zAngle *= -1;
        }
        transform.localEulerAngles = new Vector3(0, 0,zAngle); //進行方向に合わせる

        //重力
        ySpeed_ -= GRAVITY;
    }

    //当たった相手にダメージを負わすことが可能ならダメージを与える
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //当たった相手がEnemyHealthクラスを保持しているか調べる
        EnemyHealth health = collision.gameObject.GetComponent<EnemyHealth>();


        if (health != null)
        { 
        Debug.Log(collision.tag);
            health.OnDamage(); //ダメージを与える
            Destroy(this.gameObject);　//オブジェクトの破棄
        }

        Debug.Log(collision.tag);
        if (collision.gameObject.tag != "Player")
        {

            Destroy(this.gameObject);
        }
    }

    //矢が進む方向をセットする
    public void SetDirection(float direction)
    {
        xSpeed_ *= direction;
    }
}
