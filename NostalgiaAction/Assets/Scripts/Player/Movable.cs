﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//移動クラス
public class Movable : MonoBehaviour
{
    [SerializeField] float speed_ = 0.0f; //移動速度

    //移動用関数
    //引数 direction 移動したい方向
    public float Move(float directionX)
    {
        
        directionX = directionX * speed_;
        Animator animator = GetComponent<Animator>();
        
        animator.SetFloat("Speed", Mathf.Abs(directionX));
        return directionX;
    }
}
