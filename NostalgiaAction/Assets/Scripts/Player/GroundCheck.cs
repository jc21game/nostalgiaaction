﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//特定のオブジェクトと接触してるかを調べる
public class GroundCheck : MonoBehaviour
{
    private bool isGround_ = false;     //接触してるか
    private bool isGroundEnter = false; //接触し始めたか
    private bool isGroundStay = false;  //接触中か
    private bool isGroundExit = false;  //接触しなくなったか

    private string checkTag_ = ""; //接触しているか調べたいオブジェクトのタグを指定する

    //接触してるかを返す
    //引数 なし
    //戻り値 接触してるならtrueを返す
    public bool IsGround()
    {
        //接触してる
        if (isGroundEnter || isGroundStay)
        {
            isGround_ = true;
        }
        else if (isGroundExit)
        {
            //接触してない
            isGround_ = false;
        }

        //初期化
        isGroundEnter = false;
        isGroundStay = false;
        isGroundExit = false;

        return isGround_;
    }

    //接触し始め
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //接触してるか調べる
        if (collision.tag == checkTag_)
        {
            isGroundEnter = true;
        }
    }

    //接触中
    private void OnTriggerStay2D(Collider2D collision)
    {
        //接触中か調べる
        if (collision.tag == checkTag_)
        {
            isGroundStay = true;
        }
    }

    //接触してない
    private void OnTriggerExit2D(Collider2D collision)
    {   
        //接触してないか調べる
        if (collision.tag == checkTag_)
        {
            isGroundExit = true;
        }
    }

    //接触しているか調べたいオブジェクトのタグを指定する
    //引数 tagName 接触しているか調べる
    public void CheckTag(string tagName) { checkTag_ = tagName; }
}
