﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManeger : MonoBehaviour
{

    public float moveSpeed = 3f;
    public Transform attackPoint;//剣の判定を作る場所 unity上で作成
    public float attackRadius;//剣の判定の半径 unity上で作成
    public LayerMask enemyLayer;//レイヤー用変数
    Rigidbody2D rb;
    Animator animator;
    public Transform firePoint;
    //public GameObject bullet;  //弓矢の矢を生成

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Attack();
        }

    }
    void Attack()
    {
        animator.SetTrigger("AttackTrigger");//attackTriggerをonにする　unity上で作成
        Collider2D[] hitEnemys = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, enemyLayer);　//サークルの判定の中に敵がいたらそれを取得
        foreach (Collider2D hitEnemy in hitEnemys)//敵に当たったら毎回
        {
            Debug.Log(hitEnemy.gameObject.name + "に攻撃");
            hitEnemy.GetComponent<HealthManager>().OnDamage();　//EnemymanegerクラスのOnDamage関数を呼ぶ　EnemyクラスでDamage関数つくる
        }
    }

    //弓矢攻撃用関数
    /*void BowAttack()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(YumiyaPrefab, firePoint.position, transform.rotation);
        }
    }*/

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }

    void Movement()
    {
        float x = Input.GetAxisRaw("Horizontal");　//矢印キーによって動きを変えるため
        if (x > 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (x < 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        rb.velocity = new Vector2(x * moveSpeed, rb.velocity.y);　//動く速さ
        animator.SetFloat("Speed", Mathf.Abs(x));
    }
}
