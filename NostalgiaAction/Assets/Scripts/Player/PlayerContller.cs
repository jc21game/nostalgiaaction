﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerContller : MonoBehaviour
{
    //移動する方向
    const float NOT_MOVE = 0.0f; //移動しない
    const float LEFT_MOVE = -1.0f; //左方向
    const float RIGHT_MOVE = 1.0f; //右方向

    private const int attackInterval_ = 60; //攻撃できる間隔
    private int CountNextAttackable_ = 0;
    //Unityが提供するコンポーネント
    private Rigidbody2D rigidbody_ = null;
    

    //自前で実装コンポーネント
    //[SerializeField] MoveKey moveKey_ = null; //移動ボタンアクセス
    //[SerializeField] Button jumpBottun_ = null; //ジャンプボタンアクセス
    //[SerializeField] Button attackButton = null; //アタックボタンアクセス
    private Movable movable_ = null; //移動クラス
    private SimpleJump simpleJump_ = null; //ジャンプコンポーネントアクセス用
    private Attackable attack_ = null; //攻撃スクリプトアクセス用

    //弓矢で攻撃用変数
    public Transform firePoint_;     //発射場所
    public GameObject YumiyaPrefab_; //弓矢オブジェクト

    private float direcVec_ = 1; //プレイヤーが向いている方向
    // Start is called before the first frame update
    void Start()
    {
        //各コンポーネントの取得
        rigidbody_ = GetComponent<Rigidbody2D>();
        movable_ = GetComponent<Movable>();
        simpleJump_ = GetComponent<SimpleJump>();
        attack_ = GetComponent<Attackable>();

        CountNextAttackable_ = attackInterval_;
    }

    // Update is called once per frame
    void Update()
    {
        float directionX = NOT_MOVE; //x軸の移動方向 1なら右、-1なら左

        //左キーか左ボタンをクリックすると左に移動する
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.localScale = new Vector3(1, 1, 1); //キャラの向きを進行方向に変える
            directionX = LEFT_MOVE; //左に移動
            direcVec_ = LEFT_MOVE;
        }

        //右キーか右ボタンをクリックすると右に移動する
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.localScale = new Vector3(-1, 1, 1); //キャラの向きを進行方向に変える
            directionX = RIGHT_MOVE; //右に移動
            direcVec_ = RIGHT_MOVE;
        }

        //simpleJump_.SetIsJump(false);
        //ジャンプ処理用
        if (Input.GetKey(KeyCode.Space) ) simpleJump_.JumpTriggerOn();//ジャンプをするようにする

        //攻撃処理
        if (Input.GetKeyDown(KeyCode.Z) && CountNextAttackable_ >= attackInterval_)
        {
            //Debug.Log(CountNextAttackable_);
            //attack_.Attack();
            BowAttack();
            CountNextAttackable_ = 0;
        }

        //攻撃できる間隔
        CountNextAttackable_++;

        float ySpeed = simpleJump_.Jump();       //y軸座標の処理
        float xSpeed = movable_.Move(directionX);//x軸座標の処理

        //座標がきまる
        rigidbody_.velocity = new Vector2(xSpeed, ySpeed);
    }

    //弓矢攻撃用関数
    void BowAttack()
    {
        GameObject arrowObj = Instantiate(YumiyaPrefab_, firePoint_.position, transform.rotation);
        arrowObj.GetComponent<Ya>().SetDirection(direcVec_);
    }
}
