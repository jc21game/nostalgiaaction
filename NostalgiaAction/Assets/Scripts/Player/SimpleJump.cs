﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//二段ジャンプとか考慮しないシンプルなジャンプ
public class SimpleJump : MonoBehaviour
{
   // [SerializeField] Rigidbody2D rigidbody_ = null; //コンポーネントアクセス用
    [SerializeField] GroundCheck groundCheck_ = null; //地面に接着してるかを調べる
    [SerializeField] GroundCheck headCheck_ = null; //頭が何かと接触してるか

    [SerializeField] float jumpSpeed_ = 0.0f; //上方向への速度
    [SerializeField] float gravity_ = 0.0f;   //重力

    [SerializeField] float jumpHeight_ = 0.0f; //jumpPosと組み合わせて使う
    private float jumpPos_ = 0.0f;   //ジャンプしたときの高さを記録しとく

    private bool isJumping_ = false; //ジャンプ中か
    private bool isJumpTrigger_ = false; //ジャンプ処理を行うか


    //ジャンプ処理
    //この関数を呼ぶ前にJumpTrigger関数を呼ばないと
    //ジャンプしない
    //引数 なし
    //戻り値 ySpeed y軸の変化量、何もない場合はgravityの値になる
    public float Jump()
    {
        float ySpeed = -gravity_; //ジャンプしないときは重力を返す

        groundCheck_.CheckTag("Ground"); //接触してるか調べたいタグ
        headCheck_.CheckTag("Ground");
        //接触してる
        if (groundCheck_.IsGround())
        {
            if (isJumpTrigger_)
            {
                ySpeed = jumpSpeed_; //ジャンプ
                jumpPos_ = transform.position.y; //ジャンプしたときの高さ

                isJumping_ = true; //ジャンプ中
            }
            else
            {
                //ジャンプしてない
                isJumping_ = false;
            }
        }
        else
        {
            //ジャンプ中でまだジャンプできる高さなら増やす
            if (isJumping_ && jumpPos_ + jumpHeight_ > transform.position.y)
            {
                ySpeed = jumpSpeed_;
            }            
            else
            {
                //ジャンプ終わり
                isJumping_ = false;
                ySpeed = -jumpSpeed_*2; //ジャンプが終わったら早めに落下
            }

        }

        if (headCheck_.IsGround())
        {
            isJumping_ = false;
        }

     
        //トリガーの初期化
        //初期化しないとずっと飛びっぱなしになる
        isJumpTrigger_ = false; 

        return ySpeed; //どれだけy軸方向が変化するかを返す
    }


    //ジャンプさせる
    //Jump関数を使うときは使う場所でこれを先に呼ぶ
    //でないとジャンプできない
    //引数なし
    public void JumpTriggerOn()
    {
        isJumpTrigger_ = true;
    }
}
