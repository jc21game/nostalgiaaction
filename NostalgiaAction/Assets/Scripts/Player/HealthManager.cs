﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//このクラスがアッタッチされたオブジェクトの体力を管理
public class HealthManager : MonoBehaviour
{

    [SerializeField]int maxHP_ = 0; //最大hp
    private int currentHP_ = 0; //現在のhp
    Animator animator_ = null; //ダメージを受けた時のアニメを再生

    // Start is called before the first frame update
    void Start()
    {
        currentHP_ = maxHP_; //現在のhpを最大値と合わせる
        animator_ = GetComponent<Animator>();
    }


    //攻撃を受けた時の処理
    //ダメージを与えられる
    //引数 なし
    public void OnDamage()
    {
        animator_.SetTrigger("IsHurt");

        currentHP_--;
        Mathf.Clamp(currentHP_, 0, maxHP_); //バグ防止
        if (currentHP_ <= 0)
        {
            //アッタッチされているオブジェクトを削除
            //殺す
            Destroy(this.gameObject);
        }
    }

    //回復を行う
    //引数 healAmount どれだけ回復するかを指定
    public void HealHealth (int healAmount)
    {   
        //回復量ぶん回復
        //回復した後のhpを最大hpより超えないようにする
        currentHP_ += healAmount;
        Mathf.Clamp(currentHP_, 0, maxHP_);
    }

    //最大hpを取得
    //引数 なし
    //戻り値 最大hp
    public int GetMaxHP()
    {
        return maxHP_;
    }

    //現在のhpを取得
    //引数 なし
    //戻り値 現在のhp
    public int GetCurrentHP()
    {
        return currentHP_;
    }
}
