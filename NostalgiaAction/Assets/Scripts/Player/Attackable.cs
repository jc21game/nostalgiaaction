﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//攻撃を行うクラス
public class Attackable : MonoBehaviour
{

    [SerializeField] Transform attackPoint = null;//剣の判定を作る場所 unity上で作成
    [SerializeField] float attackRadius = 0.0f;//剣の判定の半径 unity上で作成
    [SerializeField] LayerMask enemyLayer;//レイヤー用変数
    private Animator animator = null;//アニメーション
    private Rigidbody2D rg_ = null;

    void Start()
    {
        //コンポーネントの取得
        rg_ = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    //呼ばれると攻撃を行う
    public void Attack()
    {
        //Debug.Log("に攻撃");
        animator.SetTrigger("AttackTrigger");//attackTriggerをonにする　unity上で作成
        Collider2D[] hitEnemys = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, enemyLayer);　//サークルの判定の中に敵がいたらそれを取得
        foreach (Collider2D hitEnemy in hitEnemys)//敵に当たったら毎回
        {
            //Debug.Log(hitEnemy.gameObject.name + "に攻撃");
            hitEnemy.GetComponent<HealthManager>().OnDamage();　//相手がHealthManagerを持っていたらのOnDamage関数を呼ぶ　EnemyクラスでDamage関数つくる
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }

    
}
