﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreText : MonoBehaviour
{
    [SerializeField] Text scoreText_; //スコアをテキストにセットための変数
    private int score_;//スコア
    // Start is called before the first frame update
    void Start()
    {
        score_ = GManager.gManagerInstance_.GetScore(); //初期化
        scoreText_.text = score_.ToString(); //テキストに登録する
    }

    // Update is called once per frame
    void Update()
    {
        //スコアの更新
        score_ = GManager.gManagerInstance_.GetScore();
        scoreText_.text = score_.ToString();
    }
}
