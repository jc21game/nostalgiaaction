﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Result : MonoBehaviour
{
    [SerializeField] Text resultScre_;
    private int score_;

    // Start is called before the first frame update
    void Start()
    {
        score_ = GManager.gManagerInstance_.GetScore();
        resultScre_.text = score_.ToString();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
