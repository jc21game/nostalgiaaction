﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBase : MonoBehaviour
{
    //Start is called before the first frame update
    //private float imgWidth_ = 0.0f;  //画像の幅
    private float imgHeight_ = 0.0f; //画像の高さ

    private float radius_;//この画像の半径

    // Start is called before the first frame update
    void Start()
    {
        //画像の幅、高さを取得
        imgHeight_ = GetComponent<SpriteRenderer>().bounds.size.y;
        radius_ = imgHeight_ / 2.0f; //半径は直径の半分
    }

    //ジャンプボタンが押されたか調べる
    //引数 なし
    //戻り値 押されたか
    public bool IsClickBottun()
    {
        //取得したマウスの座標をワールド座標に変換する処理
        //スクリーン座標をワールド座標に変換した位置座標
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePosition = Input.mousePosition;
            Vector3 mauseWorldPos = Camera.main.ScreenToWorldPoint(mousePosition); ;
            Vector3 imgPosition = transform.position;

            //タッチした場所がボタンの範囲に入っているか調べる
            float clickPosY = mauseWorldPos.y - imgPosition.y;
            float clickPosX = mauseWorldPos.x - imgPosition.x;
            float c = Mathf.Sqrt(clickPosX * clickPosX + clickPosY * clickPosY);

            if (c <= radius_)
            {
                return true; //押された
            }
           
        }
        return false; //押されてない
    }
}
