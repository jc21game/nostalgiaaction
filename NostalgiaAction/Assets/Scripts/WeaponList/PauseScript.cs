﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    [SerializeField]GameObject pauseUIPrefab_;
    private GameObject pauseUIInstance_;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            pauseUIPrefab_.SetActive(!pauseUIPrefab_.activeSelf);

            if (pauseUIPrefab_.activeSelf)
            {
                pauseUIInstance_ = GameObject.Instantiate(pauseUIPrefab_) as GameObject;
                Time.timeScale = 0f;
            }
            else
            {
                Destroy(pauseUIInstance_);
                Time.timeScale = 1f;
            }
        }
    }
}
