﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//アイテム管理画面のアイテムを作成する
public class CreateCell : MonoBehaviour
{
    [SerializeField] RectTransform contentRectTransform_;
    [SerializeField] Button button_;
 
    // Start is called before the first frame update
    void Start()
    {
        GameObject itemDataBase = GameObject.Find("ItemDataBase");
        ItemDataBase itemData = itemDataBase.GetComponent<ItemDataBase>();

        GameObject player = GameObject.Find("TestPlayer");
        ItemStatus itemStatus = player.GetComponent<ItemStatus>();

        int i = 0;
        foreach(var item in itemData.GetItemDataList())
        {
            if (itemStatus.GetItemFlag(item.GetItemName()))
            {
                //アイテムボタンを作成
                //アイテムの名前と表示する画像を取得する
                var obj = Instantiate(button_, contentRectTransform_);
                obj.GetComponentInChildren<Text>().text = item.GetItemName();
                obj.GetComponentInChildren<Image>().sprite = item.GetItemSprite();
            }
        }

        /*
        for (int i = 0; i <= 30; i++)
        {
            var obj = Instantiate(button_, contentRectTransform_);
            obj.GetComponentInChildren<Text>().text = i.ToString();
         //   var weapon = Instantiate(gameobje);
         //   obj.GetComponentInChildren<Text>().text = i.ToString();
        //}
        */
    }
}
