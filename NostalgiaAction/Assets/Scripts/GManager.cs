﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ゲームの管理を行う
//スコアの保持
public class GManager : MonoBehaviour
{
    public static GManager gManagerInstance_ = null; //ゲームマネージャーを作成
    int score_ = 0; //ゲーム中のスコア

    private void Awake()
    {
        if (gManagerInstance_ == null)
        {
            gManagerInstance_ = this;
            DontDestroyOnLoad(this.gameObject); //シーン移行時にオブジェクトの削除を行わない
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //スコアの更新
    public void ScoreSum(int score)
    {
        score_ += score;
        Debug.Log(score_);
    }

    //スコアを取得する
    //引数 なし
    //戻値 スコア
    public int GetScore()
    {
        return score_;
    }
}
