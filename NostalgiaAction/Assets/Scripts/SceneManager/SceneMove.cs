﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneMove : MonoBehaviour
{
    [SerializeField] string nextScene_ = ""; //次のシーンを設定

    public void OnClickStartButton()
    {
        SceneManager.LoadScene(nextScene_);
    }
}
