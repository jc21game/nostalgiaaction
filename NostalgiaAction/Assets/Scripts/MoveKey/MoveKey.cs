﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//左右移動キーを扱うクラス
public class MoveKey : MonoBehaviour
{
    // Start is called before the first frame update
    private float imgWidth_ = 0.0f;  //画像の幅
    private float imgHeight_ = 0.0f; //画像の高さ
 
    //最初に呼ばれる
    void Start()
    {
        //画像の幅、高さを取得
        imgWidth_  = GetComponent<SpriteRenderer>().bounds.size.x;
        imgHeight_ = GetComponent<SpriteRenderer>().bounds.size.y;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //マウスでタッチしたときの処理
    //引数 なし
    //戻り値 押されたかどうか
    public bool IsClickRightButton()
    {
        if (Input.GetMouseButton(0))
        {
            //取得したマウスの座標をワールド座標に変換する処理
            //スクリーン座標をワールド座標に変換した位置座標
            Vector3 mousePosition = Input.mousePosition;
            //Debug.Log(mousePosition);
            Vector3 mauseWorldPos = Camera.main.ScreenToWorldPoint(mousePosition); ;
            Vector3 imgPosition = transform.position;

            //画像の座標を求めるためにつかう
            float imgW = (imgWidth_ / 2.0f);
            float imgH = (imgHeight_ / 2.0f);
            
            //キーオブジェクトが押されているかを調べる
            if (mauseWorldPos.x <= imgPosition.x + imgW && mauseWorldPos.x >= imgPosition.x &&
                mauseWorldPos.y <= imgPosition.y + imgH && mauseWorldPos.y >= imgPosition.y - imgH)
            {
                return true; //押された
            }
        }

        return false; //押されてない
    }

    //マウスでタッチしたときの処理
    //引数 なし
    //戻り値 押されたかどうか
    public bool IsClickLeftButton()
    {
        if (Input.GetMouseButton(0))
        {
            //マウスボタンを押したときの座標を
            //スクリーン座標をワールド座標に変換した位置座標   
            Vector3 mauseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition); ;
            Vector3 imgPosition = transform.position; //長いので名前変える

            //何度もかくの面倒なので
            //変数に入れとく
            float imgW = (imgWidth_ / 2.0f); //オブジェクトの中心
            float imgH = (imgHeight_ / 2.0f);

            //押された範囲を調べる
            //UIとかぶったら押されたことになる
            if (mauseWorldPos.x <= imgPosition.x && mauseWorldPos.x >= imgPosition.x - imgW &&
                mauseWorldPos.y <= imgPosition.y + imgH && mauseWorldPos.y >= imgPosition.y - imgH)
            {
                return true; //左側が押された
            }
        }

        return false; //押されてない
    }

    //ネストが深すぎる改善すること
    //タッチ操作用
    //public bool IsTouchRightButton()
    //{
    //    押している指の数を調べる
    //    int touchCount = Input.touchCount;

    //    押している指がある
    //    if (touchCount > 0)
    //    {
    //        押している指全部を調べる
    //        for (int i = 0; i < touchCount; i++)
    //        {
    //            i本目の指をの情報を取得
    //            Touch touch = Input.GetTouch(i);

    //            if (touch.phase == TouchPhase.Began)
    //            {
    //                Vector3 imgPosition = transform.position; //長いので名前変える
    //                Vector3 touchPos = touch.position;
    //                //何度もかくの面倒なので
    //                //変数に入れとく
    //                float imgW = (imgWidth_ / 2.0f); //オブジェクトの中心
    //                float imgH = (imgHeight_ / 2.0f);

    //                押している指がUIの範囲内にあるか調べる
    //                if (touchPos.x <= imgPosition.x + imgW && touchPos.x >= imgPosition.x &&
    //                    touchPos.y <= imgPosition.y + imgH && touchPos.y >= imgPosition.y - imgH)
    //                {
    //                    return true; //押している
    //                }
    //            }
    //        }

    //    }
    //    return false;
    //}

    ////タッチ操作用
    //public bool IsTouchLeftButton()
    //{
    //    //押している指の数を調べる
    //    int touchCount = Input.touchCount;

    //    //押している指がある
    //    if (touchCount > 0)
    //    {
    //        //押している指全部を調べる
    //        for (int i = 0; i < touchCount; i++)
    //        {
    //            //i本目の指をの情報を取得
    //            Touch touch = Input.GetTouch(i);

    //            if (touch.phase == TouchPhase.Began)
    //            {
    //                Vector3 imgPosition = transform.position; //長いので名前変える
    //                Vector3 touchPos = touch.position;
    //                ////何度もかくの面倒なので
    //                ////変数に入れとく
    //                float imgW = (imgWidth_ / 2.0f); //オブジェクトの中心
    //                float imgH = (imgHeight_ / 2.0f);

    //                //押している指がUIの範囲内にあるか調べる
    //                if (touchPos.x <= imgPosition.x && touchPos.x >= imgPosition.x - imgW &&
    //                    touchPos.y <= imgPosition.y + imgH && touchPos.y >= imgPosition.y - imgH)
    //                {
    //                    return true; //押している
    //                }
    //            }
    //        }

    //    }
    //    return false;
    //}
}
