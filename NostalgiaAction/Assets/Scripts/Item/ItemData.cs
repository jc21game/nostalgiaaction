﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//一つ一つのアイテムが持つ情報をまとめる
public class ItemData
{
    private Sprite itemSprite_; //アイテムの画像
    private string itemName_; //アイテムの名前
    private ItemDataBase.ItemType itemType_; //アイテムのタイプ
    private string itemInformation_; //アイテムの情報

    //コンストラクタ
    //引数 image アイテムの画像
    //引数 itemName アイテムの名前
    //引数 itemType アイテムの種類
    //引数 infrmation アイテムの基本情報
    public ItemData(Sprite image, string itemName, ItemDataBase.ItemType itemType, string information)
    {
        //private変数の初期化
        itemSprite_ = image;
        itemName_ = itemName;
        itemType_ = itemType;
        itemInformation_ = information;
    }

    //アイテムの画像を返す
    public Sprite GetItemSprite()
    {
        return itemSprite_;
    }

    //アイテムの名前を返す
    public string GetItemName()
    {
        return itemName_;
    }

    //アイテムの種類を返す
    public ItemDataBase.ItemType GetItemType()
    {
        return itemType_;
    }

    //アイテムの基本情報を返す
    public string GetItemInformation()
    {
        return itemInformation_;
    }
}
