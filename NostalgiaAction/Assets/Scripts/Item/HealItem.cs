﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealItem : MonoBehaviour
{
     
    private int healAmount_ = 1; //回復量
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //接触し始め
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //接触してるか調べる
        if (collision.tag == "Player")
        {
            //体力を回復
            HealthManager health = collision.GetComponent<HealthManager>();
            health.HealHealth(healAmount_);
            Destroy(this.gameObject);
        }
    }
}
