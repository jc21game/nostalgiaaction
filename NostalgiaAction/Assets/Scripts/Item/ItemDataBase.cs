﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//すべてのアイテムを管理
public class ItemDataBase : MonoBehaviour
{
    //アイテムの種類
    public enum ItemType
    {
        Weapon
    }

    //アイテムデータのリスト
    //アイテムを追加していく
    public List<ItemData> itemDataList_ = new List<ItemData>();

    private void Awake()
    {
        //アイテムの全情報を作成
        itemDataList_.Add(new ItemData(Resources.Load("CupSword", typeof(Sprite)) as Sprite, "銅の剣", ItemType.Weapon, "銅の剣"));
        itemDataList_.Add(new ItemData(Resources.Load("FeSword", typeof(Sprite)) as Sprite, "銀の剣", ItemType.Weapon, "銀の剣"));
    }

    //アイテムリストを渡す
    public List<ItemData>GetItemDataList()
    {
        return itemDataList_;
    }

    //引数で指定したアイテムを返す
    //引数 itemName 取得したいアイテムの名前
    public ItemData GetItemData(string itemName)
    {
        //名前がitemNameのアイテムがないかリスト内を探査
        //あったらアイテムを返す
        foreach(var item in itemDataList_)
        {
            if (item.GetItemName() == itemName)
            {
                return item;
            }
        }

        return null;
    }
}
