﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemStatus : MonoBehaviour
{
    //アイテムを持っているかどうかのフラグ
    private Dictionary<string, bool> itemFlags_ = new Dictionary<string, bool>();
    [SerializeField] ItemDataBase itemDataBase_; //アイテムデータベース

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーが持っているアイテムの初期化
        foreach(var item in itemDataBase_.GetItemDataList())
        { 
            itemFlags_.Add(item.GetItemName(), false);
        }

        itemFlags_["銅の剣"] = true;
        itemFlags_["銀の剣"] = true;
    }

    //アイテムを持っているか調べる
    //引数 itemName 持っているか調べたいアイテムの名前
    //戻値 持っていればtureを返す
    public bool GetItemFlag(string itemName)
    {
        return itemFlags_[itemName];
    }

    private void OnCollisionEnter(Collision collision)
    {

    }
}
