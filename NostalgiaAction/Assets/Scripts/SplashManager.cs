﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

//動画が終了したらタイトルシーンに遷移する
public class SplashManager : MonoBehaviour
{
    [SerializeField] VideoPlayer videoPlayer_;//ビデオプレイヤーアクセス用

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer_.isLooping = true; //必ずループ再生になるようにする
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        videoPlayer_.loopPointReached += EndReached;
    }

    private void OnDisable()
    {
        videoPlayer_.loopPointReached -= EndReached;
    }

    //動画が終わったらタイトルシーンに遷移する
    //引数 vp アタッチされたVideoPlayer
    void EndReached(VideoPlayer vp)
    {
        SceneManager.LoadScene("TitleScene");
    }
}
