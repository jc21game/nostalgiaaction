﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Enemymaneger : MonoBehaviour
{
    Animator animator;
    HealthManager healthManager_;
    public Transform attackPoint;//剣の判定を作る場所 unity上で作成
    public float attackRadius;//剣の判定の半径 unity上で作成
    public LayerMask enemyLayer;//レイヤー用変数
    Rigidbody2D rb;
    float attackInterval_;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        attackInterval_ = 0.0f;
    }

    private void Update()
    {
        
        if (attackInterval_ % 60.0f == 0)
        {
            Attack();
        }
        
        attackInterval_++;
       
    }

    public void OnDamage()
    {
        animator.SetTrigger("IsHurt");
    }

    void Attack()
    {
        animator.SetTrigger("AttackTrigger");//attackTriggerをonにする　unity上で作成
        Collider2D[] hitEnemys = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, enemyLayer);　//サークルの判定の中に敵がいたらそれを取得
        foreach (Collider2D hitEnemy in hitEnemys)//敵に当たったら毎回
        {
            Debug.Log(hitEnemy.gameObject.name + "に攻撃");
            hitEnemy.GetComponent<HealthManager>().OnDamage();　//EnemymanegerクラスのOnDamage関数を呼ぶ　EnemyクラスでDamage関数つくる
        }
    }

}
