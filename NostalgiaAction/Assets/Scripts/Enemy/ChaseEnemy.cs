﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseEnemy : MonoBehaviour
{
    //インスペクターで設定する
    [Header("移動速度")] public float speed;
    //[Header("重力")] public float gravity;//スクリプト制御ならGravity Scaleを0に
    [Header("画面外でも行動する")] public bool nonVisibleAct;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;

    //プライベート変数
    private GameObject player;  //プレイヤー取得
    private Rigidbody2D rb = null;
    private SpriteRenderer sr = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private BoxCollider2D col = null;
    private bool rightTleftF = false;   //移動方向
    private bool isDead = false; //死んだ

    private Vector2 bacePos_; //この座標を中心に移動する
    private Vector2 moveVec_ = new Vector2(-1,0); //移動方向
    private Vector2 prevPos_;
    [SerializeField]BoxCollider2D norse_;
    [SerializeField] GroundCheck groundCheck_;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>(); //画面内にいるか
        anim = GetComponent<Animator>();
        oc = GetComponent<ObjectCollision>();
        col = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectsWithTag("Player")[0];
        bacePos_ = transform.position; //オブジェクトが生成したときのxy座標を記憶

        //オブジェクトの反転
        transform.localScale = new Vector3(-1*moveVec_.x, 1, 1);

        prevPos_ = transform.position;
    }

    void FixedUpdate()
    {
        groundCheck_.CheckTag("Ground");
        if (groundCheck_.IsGround())
        {
            rb.gravityScale = 0;
        }
        else
        {
            rb.gravityScale = 5f;
        }
        if (sr.isVisible || nonVisibleAct)  //画面内or画面外
        {
            

            if (!checkCollision.isOn)    //接触判定に触れてない
            {
            }
            else
            {
                Debug.Log("接触");
            }

            Move();
        }
        else
        {
            rb.Sleep();     //物理演算停止
        }
    }

    //移動処理
    void Move()
    {
        //移動方向の反転
        if (transform.position.x > bacePos_.x + 2f)
        {
            moveVec_.x *= -1; //反転する
        }
        if (transform.position.x < bacePos_.x -2f)
        {
            moveVec_.x *= -1;
        }

        //鼻が地面と接触したら反転
        int rayerMask = LayerMask.GetMask("Ground");
        if (norse_.IsTouchingLayers(rayerMask))
        {
            moveVec_.x *= -1;
        }

        //オブジェクトの反転
        transform.localScale = new Vector3(-1 * moveVec_.x,1,1);

        //移動
        rb.velocity = moveVec_ * 0.5f;
    }

    //接触し始め
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //接触してるか調べる
        if (collision.tag == "Player")
        {
            HealthManager healthManager = collision.gameObject.GetComponent<HealthManager>();
            healthManager.OnDamage();
        }
    }
}