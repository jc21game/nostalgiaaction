﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敵を管理する
public class EnemyManager : MonoBehaviour
{
    [SerializeField] Rigidbody2D rigidbody_ = null; //物理計算
    [SerializeField] Movable movable_ = null; //移動用
   // [SerializeField] BoxCollider2D boxCollider_ = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //とりあえず移動しない
        float xSpeed = movable_.Move(0);
        rigidbody_.velocity = new Vector2(xSpeed,-10.0f);
    }

    //接触し始め
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //接触してるか調べる
        if (collision.tag == "Player")
        {            
            HealthManager health = collision.GetComponent<HealthManager>();
            health.OnDamage();
        }
    }

}
