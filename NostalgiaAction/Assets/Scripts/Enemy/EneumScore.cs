﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//スコアを加算するクラス
public class EneumScore : MonoBehaviour
{
    [SerializeField]int socre_ = 100; //インスペクターでスコアの設定を行う

    //アタッチされているオブジェクトが破棄されたらスコアを加算する
    private void OnDestroy()
    {
        GManager.gManagerInstance_.ScoreSum(socre_);
    }
}
