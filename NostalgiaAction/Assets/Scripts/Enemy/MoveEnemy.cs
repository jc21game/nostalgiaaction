﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{
    //インスペクターで設定する
    [Header("移動速度")] public float speed;
    [Header("重力")] public float gravity;//スクリプト制御ならGravity Scaleを0に
    [Header("画面外でも行動する")] public bool nonVisibleAct;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;
    //プライベート変数
    private Rigidbody2D rb = null;
    private SpriteRenderer sr = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private CircleCollider2D col = null;
    private bool rightTleftF = false;   //移動方向
    private bool isDead = false; //死んだ

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>(); //画面内にいるか
        anim = GetComponent<Animator>();
        oc = GetComponent<ObjectCollision>();
        col = GetComponent<CircleCollider2D>();
    }

    void FixedUpdate()
    {
        if (!oc.playerStepOn) //踏まれてない
        {
            //Debug.Log("移動");

            if (sr.isVisible || nonVisibleAct)  //画面内or画面外
            {

                if (checkCollision.isOn)    //接触判定に触れた
                {

                    rightTleftF = !rightTleftF;//反転処理
                }
                int xVector = -1;
                if (rightTleftF)    //trueで右に進む
                {
                    xVector = 1;
                    transform.localScale = new Vector3(-1, 1, 1);
                }
                else
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }
                rb.velocity = new Vector2(xVector * speed, -gravity);
            }
            else
            {
                rb.Sleep();     //物理演算停止
            }
        }
        else   //踏まれた
        {
            Debug.Log("死ぬ");
            if (checkCollision.isDead)
            {
                Debug.Log("死");
                Debug.Log(checkCollision.isDead);
                // anim.Play("dead");  //死亡モーション
                // rb.velocity = new Vector2(0, -gravity);
                //  isDead = true;
                //  col.enabled = false;    //接触判定オフ
                // Destroy(gameObject, 3f);    //3秒後破棄
            }
            else
            {
                //死ぬまでの間 
            }
        }
    }
}