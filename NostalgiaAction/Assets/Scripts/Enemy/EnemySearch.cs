﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySearch : MonoBehaviour
{
    /// <summary>
    /// 判定内に自機がある
    /// </summary>
    [HideInInspector] public bool isIn = false; //自機が円の内側にいるか否か 
    [HideInInspector] public bool isInR = false; //自機が円の右側にいるか否か
    [HideInInspector] public bool isInL = false; //自機が円の左側にいるか否か

    //座標を取得するオブジェクト
    public GameObject obj;
    //プレイヤーのタグ付きを探す
    private string playerTag = "Player";
    //座標変数
    private float dir;

    //サークルに入った
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == playerTag)
        {
            //内側に入った
            isIn = true;
            Debug.Log("オブジェクトを発見");

            //オブジェクトの位置ー自分（敵）の位置
            Vector3 dir = obj.transform.position - transform.position;
            float DIR_Y = dir.y;
            float DIR_LR = dir.x;         

            //敵より低い位置にプレイヤーがいない
            if (DIR_Y > 0)  //Debug.Log("オブジェクトは上");
            {                
                //自分よりプレイヤーが右か左か
                if (DIR_LR > 0)
                {
                    //Debug.Log("オブジェクトは右");
                    isInR = true;
                    isInL = false;
                }
                else if (DIR_LR < 0)
                {
                    //Debug.Log("オブジェクトは左");
                    isInL = true;
                    isInR = false;
                }
            }
            else
            {// Debug.Log("オブジェクトは下");
                isIn = false;
            }
        }
    }


    //サークルから出た
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == playerTag)
        {
            Debug.Log("オブジェクトを見失う");
            isIn = false;
            isInR = false;
            isInL = false;
        }
    }
}