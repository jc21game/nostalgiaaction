﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolEnemy : MonoBehaviour
{
    //インスペクターで設定する
    [Header("移動速度")] public float speed;
    [Header("重力")] public float gravity;//スクリプト制御ならGravity Scaleを0に
    [Header("追跡速度")] public float dash;
    [Header("画面外でも行動する")] public bool nonVisibleAct;
    [Header("接触判定")] public EnemyCollisionCheck checkCollision;
    [Header("自機感知")] public EnemySearch checkObject;
    //プライベート変数
    private Rigidbody2D rb = null;
    private SpriteRenderer sr = null;
    private Animator anim = null;
    private ObjectCollision oc = null;
    private CircleCollider2D col = null;
    private bool rightTleftF = false;   //移動方向
    private bool isDead = false; //死んだ
    private float speed_a = 0; //最終的な速度

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>(); //画面内にいるか
        anim = GetComponent<Animator>();
        oc = GetComponent<ObjectCollision>();
        col = GetComponent<CircleCollider2D>();
    }

    void FixedUpdate()
    {
        if (!oc.playerStepOn) //踏まれてない
        {
            if (sr.isVisible || nonVisibleAct)  //画面内or画面外
            {
                speed_a = speed;
                if (checkObject.isIn)
                {
                    Debug.Log("オブジェクト察知中");
                    int xVector_ = -1;
                    if (checkObject.isInR)    //trueで右に進む
                    {
                        xVector_ = 1;
                        transform.localScale = new Vector3(-1, 1, 1);
                    }
                    if (checkObject.isInL)
                    {
                        transform.localScale = new Vector3(1, 1, 1);
                    }
                    //察知方向に加速
                    speed_a = dash * -xVector_;
                    Debug.Log(speed_a);
                }

                if (checkObject.isIn == false && checkCollision.isOn)    //接触判定に触れた
                {
                    Debug.Log("オブジェクト");
                    rightTleftF = !rightTleftF;//反転処理
                }
                int xVector = -1;
                if (rightTleftF)    //trueで右に進む
                {
                    xVector = 1;
                    transform.localScale = new Vector3(-1, 1, 1);
                }
                else
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }
                rb.velocity = new Vector2(xVector * speed_a, -gravity);
            }
            else
            {
                rb.Sleep();     //物理演算停止
            }
        }
        else   //踏まれた
        {
            Debug.Log("死ぬ");
            if (checkCollision.isDead)
            {
                Debug.Log("死");
                Debug.Log(checkCollision.isDead);
                // anim.Play("dead");  //死亡モーション
                // rb.velocity = new Vector2(0, -gravity);
                //  isDead = true;
                //  col.enabled = false;    //接触判定オフ
                // Destroy(gameObject, 3f);    //3秒後破棄
            }
            else
            {
                //死ぬまでの間 
            }
        }
    }
}