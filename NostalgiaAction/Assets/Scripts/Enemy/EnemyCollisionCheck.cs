﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionCheck : MonoBehaviour
{
    /// <summary>
    /// 判定内に敵か壁がある
    /// </summary>
    [HideInInspector] public bool isOn = false;
    [HideInInspector] public bool isDead = false;

    private string groundTag = "Ground";
    private string enemyTag = "Enemy";
    private string playerTag = "Player";


    //接触判定
    private void OnTriggerEnter2D(Collider2D collision)
    {//壁か敵に触れた
        Debug.Log("接触");
        if (collision.tag == groundTag || collision.tag == enemyTag|| collision.tag == playerTag)
        {
            Debug.Log("接触");
            isOn = true;
        }

        if (collision.tag == playerTag)
        {
            Debug.Log("接敵");
            isDead = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == groundTag || collision.tag == enemyTag || collision.tag == playerTag)
        {
            isOn = false;
        }
    }
}
