﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollision : MonoBehaviour
{

    [Header("これを踏んだ時のプレイヤーが跳ねる高さ")] public float boundHeight;

    /// <summary>
    /// このオブジェクトをプレイヤーが踏んだかどうか
    /// →踏んだオブジェクトに取得させる
    /// </summary>
    [HideInInspector] public bool playerStepOn; //変数隠し
}