﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCaharacter : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //接触し始め
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //接触してるか調べる
        if (collision.tag == "Player")
        {
            HealthManager health = collision.GetComponent<HealthManager>();
            health.OnDamage();
        }
    }
}
