﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HPBar : MonoBehaviour
{
    [SerializeField]Slider slider_; //スライダーにアクセス
    //[SerializeField]GameObject player_; //プレイヤーにアクセス

    // Start is called before the first frame update
    void Start()
    {
        //プレイヤーの最大hpを登録
        GameObject player = GameObject.Find("TestPlayer");
        HealthManager healthManger = player.GetComponent<HealthManager>();
        slider_.maxValue = healthManger.GetMaxHP(); //hpの最大数を設定
        slider_.value = slider_.maxValue;//現在のhpを最大hpに合わせる
    }

    // Update is called once per frame
    void Update()
    {
        //GameObject player = GameObject.Find("Player");

        //現在のhpに更新
        GameObject player = null;
        player = GameObject.Find("TestPlayer");

        if (!player)
        {
            SceneManager.LoadScene("GameOverScene");
        }

        HealthManager healthManger = player.GetComponent<HealthManager>();
        slider_.value = healthManger.GetCurrentHP();

    }

}
