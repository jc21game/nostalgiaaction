﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeGameOver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PushRetryButton()   //リトライボタンを押したら
    {
        SceneManager.LoadScene("GameScene");   //シーンマネージャのロードシーンを使ってゲームシーンへ
    }

    public void PushGiveUpButton()  //タイトルに戻るボタンを押したら
    {
        SceneManager.LoadScene("TitleScene");   //シーンマネージャのロードシーンを使ってタイトルシーンへ
    }
}
