﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeToGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("ChangeScene", 1.0f);    //1秒経ったらタイトルシーンからゲームシーンへ移行
    }

    // Update is called once per frame
    void Update()
    {
    }

    void ChangeScene()
    {
        SceneManager.LoadScene("GameScene");   //シーンマネージャのロードシーンを使ってゲームシーンへ
    }
}
